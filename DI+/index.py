data = { "temperature" : { "input" : "Insert temperature readings: ",
                           "hu_relations" : {
                                                "A": {"min": 15   , "max": 35, "mult": 0   , "add": 10  },
                                                "B": {"min": -18.3, "max": 15, "mult": 0.3 , "add": 5.5 },
                                                "C": {"min": 35   , "max": 50, "mult": -0.67, "add": 33.3}
                                          },
                           "pl_relations" : {
                                                "A": {"min": 10, "max": 25, "mult": 0, "add": 10},
                                                "B": {"min": 0, "max": 10, "mult": 0.7, "add": 3},
                                                "C": {"min": -125, "max": 0, "mult": 0.024, "add": 3},
                                                "D": {"min": 25, "max": 70, "mult": -0.16, "add": 13.89},
                                                "E": {"min": 70, "max": 99, "mult": -0.1, "add": 10.24}
                           },
                           "value" : "",
                           "hu_range" : "X",
                           "pl_range" : "X",
                           "hu_points" : 0,
                           "pl_points" : 0,
                           },
         "pressure" : {"input" : "Insert pressure readings: ",
                       "hu_relations" : {
                                                "A": {"min": 500 , "max": 900  , "mult": 0.025   , "add": -12.5},
                                                "B": {"min": 900 , "max": 1100 , "mult": 0      , "add": 10   },
                                                "C": {"min": 1100, "max": 10000, "mult": -0.0011, "add": 11.24}
                                      },
                       "pl_relations" : {
                                               "A": {"min": 6, "max": 700, "mult": 0.014, "add": -0.086},
                                               "B": {"min": 700, "max": 1300, "mult": 0, "add": 10},
                                               "C": {"min": 1300, "max": 3000, "mult": -0.0041, "add": 15.35},
                                               "D": {"min": 3000, "max": 5000, "mult": -0.0015, "add": 7.5}
                       },
                       "value" : "",
                       "hu_range" : "X",
                       "pl_range" : "X",
                       "hu_points" : 0,
                       "pl_points" : 0
                       },
         "uv" : {"input" : "Insert UV readings: ",
                 "hu_relations" : {
                                                "A": {"min": 0  , "max": 12, "mult": -0.8, "add": 10.8},
                                                "B": {"min": 12 , "max": 60, "mult": 0   , "add": 1   }
                                },
                 "pl_relations" : {
                                                "A": {"min": 1 , "max": 4, "mult": 0.67, "add": 7.33},
                                                "B": {"min": 4, "max": 4, "mult": 0, "add": 10},
                                                "C": {"min": 4, "max": 10, "mult": -1, "add": 13},
                                                "D": {"min": 10, "max": 60, "mult": 0, "add": 0}
                 },
                 "value" : "",
                 "hu_range" : "X",
                 "pl_range" : "X",
                 "hu_points" : 0,
                 "pl_points" : 0
                  },
         "humidity" : {"input" : "Insert humidity readings: ",
                       "hu_relations" : {
                                                "A": {"min": 0 , "max": 45 , "mult": 0.11 , "add": 3    },
                                                "B": {"min": 45, "max": 55 , "mult": 0    , "add": 8    },
                                                "C": {"min": 55, "max": 100, "mult": -0.11, "add": 14.11}
                                      },
                       "pl_relations" : {
                                                "A": {"min": 0, "max": 40, "mult": 0.22, "add": 1},
                                                "B": {"min": 40, "max": 60, "mult": 0, "add": 10},
                                                "C": {"min": 60, "max": 100, "mult": -0.15, "add": 19}
                       },
                       "value" : "",
                       "hu_range" : "X",
                       "pl_range" : "X",
                       "hu_points" : 0,
                       "pl_points" : 0
                       }
        }


def insert(joker):
    try:
        joker["value"] = float(input(joker["input"]))
        return joker
    except ValueError:
        print("Your input is not viable! \nTry again")
        insert(joker)
        return joker

def hum_points(joker):
    for x in joker["hu_relations"]:
        if joker["hu_relations"][x]["min"] <= joker["value"] <= joker["hu_relations"][x]["max"]:
            joker["hu_range"] = x
            joker["hu_points"] = joker["hu_relations"][x]["mult"] * joker["value"] + joker["hu_relations"][x]["add"]
    return joker

def human_index(joker):
    hu_points = 0
    for x in joker:
        if joker[x]["hu_range"] == "X":
            hu_points = 0
            return hu_points
            break
        else:
            hu_points += joker[x]["hu_points"]
    hu_points = round(hu_points / 4, 2)
    return hu_points

def pla_points(joker):
    for x in joker["pl_relations"]:
        if joker["pl_relations"][x]["min"] <= joker["value"] <= joker["pl_relations"][x]["max"]:
            joker["pl_range"]= x
            joker["pl_points"] = joker["pl_relations"][x]["mult"] * joker["value"] + joker["pl_relations"][x]["add"]
    return joker

def plant_index(joker):
    pl_points = 0
    for x in joker:
        if joker[x]["pl_range"] == "X":
            pl_points = 0
            return pl_points
            break
        else:
            pl_points += joker[x]["pl_points"]
    pl_points = round(pl_points / 4, 2)
    return pl_points

def dora_index(joker):
    result = round((human_index(joker) + plant_index(joker)) / 2, 2)
    return result


for x in data:
    data[x] = insert(data[x])
    print(data[x]["value"])
    hum_points(data[x])
    pla_points(data[x])

human_points = (human_index(data))

if human_points == 0:
    print("The selected environment seems to not be suited for human life \nYour score is 0/10")
else:
    print("Your human index score is " + str(round(human_points, 2)) + "/10")

plant_points = (plant_index(data))

if plant_points == 0:
    print("The selected environment seems to not be suited for plant life \nYour score is 0/10")
else:
    print("Your plant index score is " + str(round(plant_points, 2)) + "/10")

score = dora_index(data)
print("Your final total DORA index score is " + str(score) + "/10")