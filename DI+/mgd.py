import socket
import time
import tkinter as tk
from tkinter import *
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('169.254.212.109', 50000))
output = open("data.txt" , "a+")

data = { "temperature" : {"hu_relations" : {
                                                "A": {"min": 15   , "max": 35, "mult": 0   , "add": 10 },
                                                "B": {"min": -18.3, "max": 15, "mult": 0.3 , "add": 5.5 },
                                                "C": {"min": 35   , "max": 50, "mult": -0.67, "add": 33.3 }
                                          },
                           "pl_relations" : {
                                                "A": {"min": 10, "max": 25, "mult": 0, "add": 10 },
                                                "B": {"min": 0, "max": 10, "mult": 0.7, "add": 3 },
                                                "C": {"min": -125, "max": 0, "mult": 0.024, "add": 3 },
                                                "D": {"min": 25, "max": 70, "mult": -0.16, "add": 13.89 },
                                                "E": {"min": 70, "max": 99, "mult": -0.1, "add": 10.24 }
                           },
                           "value" : "",
                           "hu_range" : "X",
                           "pl_range" : "X",
                           "hu_points" : 0,
                           "pl_points" : 0,
                           },
         "pressure" : {"hu_relations" : {
                                                "A": {"min": 500 , "max": 900  , "mult": 0.025   , "add": -12.5 },
                                                "B": {"min": 900 , "max": 1100 , "mult": 0      , "add": 10 },
                                                "C": {"min": 1100, "max": 10000, "mult": -0.0011, "add": 11.24 }
                                      },
                       "pl_relations" : {
                                               "A": {"min": 6, "max": 700, "mult": 0.014, "add": -0.086 },
                                               "B": {"min": 700, "max": 1300, "mult": 0, "add": 10 },
                                               "C": {"min": 1300, "max": 3000, "mult": -0.0041, "add": 15.35 },
                                               "D": {"min": 3000, "max": 5000, "mult": -0.0015, "add": 7.5 }
                       },
                       "value" : "",
                       "hu_range" : "X",
                       "pl_range" : "X",
                       "hu_points" : 0,
                       "pl_points" : 0
                       },
         "humidity" : {"hu_relations" : {
                                                "A": {"min": 0 , "max": 45 , "mult": 0.11 , "add": 3     },
                                                "B": {"min": 45, "max": 55 , "mult": 0    , "add": 8     },
                                                "C": {"min": 55, "max": 100, "mult": -0.11, "add": 14.11 }
                                      },
                       "pl_relations" : {
                                                "A": {"min": 0, "max": 40, "mult": 0.22, "add": 1 },
                                                "B": {"min": 40, "max": 60, "mult": 0, "add": 10 },
                                                "C": {"min": 60, "max": 100, "mult": -0.15, "add": 19 }
                       },
                       "value" : "",
                       "hu_range" : "X",
                       "pl_range" : "X",
                       "hu_points" : 0,
                       "pl_points" : 0
                       },
         "uv" : {"hu_relations" : {
                                                "A": {"min": 1  , "max": 12, "mult": -0.8, "add": 10.8 },
                                                "B": {"min": 12 , "max": 60, "mult": 0   , "add": 1    },
                                                "C": {"min": 0 , "max": 1 , "mult": 0   , "add": 10   }
                                },
                 "pl_relations" : {
                                                "A": {"min": 0 , "max": 4, "mult": 0.67, "add": 7.33 },
                                                "B": {"min": 4, "max": 4, "mult": 0, "add": 10 },
                                                "C": {"min": 4, "max": 10, "mult": -1, "add": 13 },
                                                "D": {"min": 10, "max": 60, "mult": 0, "add": 0 }
                 },
                 "value" : "",
                 "hu_range" : "X",
                 "pl_range" : "X",
                 "hu_points" : 0,
                 "pl_points" : 0
                  },
         "CO2" : {"hu_relations" : {
                                                "A": {"min": 1  , "max": 300, "mult": 0.0067, "add": 8 },
                                                "B": {"min": 300 , "max": 1500, "mult": -0.0033, "add": 11 },
                                                "C": {"min": 1500 , "max": 3500, "mult": -0.001, "add": 7.5 },
                                                "D": {"min": 3500 , "max": 4000, "mult": -0.00011, "add": 4.38}
                                },
                 "pl_relations" : {
                                                "A": {"min": 150 , "max": 5000, "mult": 0.0016, "add": -0.25 },
                                                "B": {"min": 5000 , "max": 1000000 , "mult": 0, "add": 8}
                 },
                 "value" : 500,
                 "hu_range" : "X",
                 "pl_range" : "X",
                 "hu_points" : 0,
                 "pl_points" : 0
                  }
        }



def hum_points(joker):
    for x in joker["hu_relations"]:
        if joker["hu_relations"][x]["min"] <= joker["value"] <= joker["hu_relations"][x]["max"]:
            joker["hu_range"] = x
            joker["hu_points"] = joker["hu_relations"][x]["mult"] * joker["value"] + joker["hu_relations"][x]["add"]
    return joker

def human_index(joker):
    hu_points = 0
    for x in joker:
        if joker[x]["hu_range"] == "X":
            hu_points = 0
            break
        else:
            hu_points += joker[x]["hu_points"]
    hu_points = round(hu_points / len(joker), 2)
    return hu_points

def pla_points(joker):
    for x in joker["pl_relations"]:
        if joker["pl_relations"][x]["min"] <= joker["value"] <= joker["pl_relations"][x]["max"]:
            joker["pl_range"]= x
            joker["pl_points"] = joker["pl_relations"][x]["mult"] * joker["value"] + joker["pl_relations"][x]["add"]
    return joker

def plant_index(joker):
    pl_points = 0
    for x in joker:
        if joker[x]["pl_range"] == "X":
            pl_points = 0
            break
        else:
            pl_points += joker[x]["pl_points"]
    pl_points = round(pl_points / len(joker), 2)
    return pl_points

def dora_index(joker):
    result = round((human_index(joker) + plant_index(joker)) / 2, 2)
    return result

root = tk.Tk()
root.geometry('640x480')
root.title('Di+')
titlelbl = tk.Label(root, text= "DORA index +", font= ('Courier', 30))
titlelbl.pack()
scrollbar = Scrollbar(root)
scrollbar.pack(side=RIGHT, fill=Y)
listbox = Listbox(root)
listbox.pack(expand=1,fill='both')

while True:
    while True:
        packet = (s.recv(1024)).decode('utf-8')
        time.sleep(0.5)
        try:
            packet = packet.lstrip("D,")
            readings = packet.split(",")
            temp = readings[0]
            pre = readings[1]
            hum = readings[2]
            uv = readings[3]
            break
        except ValueError:
            listbox.insert(END, "..." )
            root.update()

    uv = uv * 10

    data["temperature"]["value"] = temp
    data["pressure"]["value"] = pre
    data["humidity"]["value"] = hum
    data["uv"]["value"] = uv

    listbox.insert(END, "Temperature: " + str(data["temperature"]["value"]) + " C , Pressure: " + str(data["pressure"]["value"]) + " Pa , Humidity: " + str(data["humidity"]["value"]) + " % RH , UV: level " + str(data["uv"]["value"]) + " UV index , CO2: " + str(data["CO2"]["value"]) + " ppm")

    for x in data:
        hum_points(data[x])
        pla_points(data[x])

    human_points = (human_index(data))
    plant_points = (plant_index(data))

    score = dora_index(data)

    output.write(packet)
    output.write(str(score))
    output.flush()

    listbox.insert(END, "DORA index score is: " + str(score))
    root.update()
