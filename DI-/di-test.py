import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.tree import export_graphviz
import pydot

# label on first row of csv file

ds=pd.read_csv('log.csv')
print(ds)

ds=ds.drop(columns=['D','lat','long','alt','sol','bat','speed'])
print(ds)

print(ds.describe())

target = np.array(ds['di+'])

print(target)

train_features = ds.drop(columns="di+")
train_features = np.array(train_features)
print(train_features)

rf = RandomForestRegressor(n_estimators=3000,random_state=1)

rf.fit(train_features, target)

test=np.array([[-30,500,0,10],[100,1000,50,3]])
print(test)

predictions = rf.predict(test)
print(predictions)

tree = rf.estimators_[5]

export_graphviz(tree, out_file = 'tree.dot', feature_names = ['temp','pres','hum','uv'], rounded = True, precision = 1)
(graph, ) = pydot.graph_from_dot_file('tree.dot')
graph.write_png('tree.png')
