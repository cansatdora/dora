from tkinter import *
from math import sqrt, pi


def calculate():
    try:
        mass = float(entry_m.get())
        acc = float(entry_g.get())
        vel = float(entry_v.get())
        rho = float(entry_r.get())
        cd = float(entry_c.get())

        area = "Area = " + str(round((2*mass*acc)/(rho*cd*(vel**2)), 4))+" m^2"
        diam = "Diameter = " + str(round(sqrt((8*mass*acc)/(pi*rho*cd*(vel**2))), 4))+" m"
        label_area.configure(text=area)
        label_diam.configure(text=diam)
    except:
        label_area.configure(text="Area = ERROR: INVALID INPUT")
        label_diam.configure(text="Diameter = ERROR: INVALID INPUT")


root = Tk()

root.title("DORA PARACHUTE CALCULATOR")
root.configure(bg="white")
#root.iconbitmap(default='dora.ico')

label_title = Label(root, text="Dora Parachute Calculator", font=("Century Gothic Bold", 30), bg="white", fg="#1E10ff")

label_m = Label(root, text="m[kg] = ", font=("Century Gothic", 20), bg="white", fg="#1e10ff")
label_g = Label(root, text="g[m/s^2] = ", font=("Century Gothic", 20), bg="white", fg="#1e10ff")
label_v = Label(root, text="v[m/s] = ", font=("Century Gothic", 20), bg="white", fg="#1e10ff")
label_r = Label(root, text="ρ[kg/m^3] = ", font=("Century Gothic", 20), bg="white", fg="#1e10ff")
label_c = Label(root, text="Cd = ", font=("Century Gothic", 20), bg="white", fg="#1e10ff")

label_area = Label(root, text="Area =             ", font=("Century Gothic Bold", 20), bg="white")
label_diam = Label(root, text="Diameter =             ", font=("Century Gothic Bold", 20), bg="white")

label_foot = Label(root, text="Made by Jakub Korycki from the DORA team for the 2019 CanSat Belgium competition", font=("Century Gothic", 10), bg="white", fg="grey")

entry_m = Entry(root, width=8, font=("Century Gothic", 20), bd=0, highlightthickness=0, bg="#F4F8F7")
entry_g = Entry(root, width=8, font=("Century Gothic", 20), bd=0, highlightthickness=0, bg="#F4F8F7")
entry_v = Entry(root, width=8, font=("Century Gothic", 20), bd=0, highlightthickness=0, bg="#F4F8F7")
entry_r = Entry(root, width=8, font=("Century Gothic", 20), bd=0, highlightthickness=0, bg="#F4F8F7")
entry_c = Entry(root, width=8, font=("Century Gothic", 20), bd=0, highlightthickness=0, bg="#F4F8F7")

btn_calc = Button(root, pady=1, text="Calculate", width=10, font=("Century Gothic Bold", 20), relief="flat", highlightthickness=0, command=calculate, fg="white", bg="#1e10ff", highlightbackground='#1e10ff')

label_title.grid(row=0, columnspan=3)

label_m.grid(row=1)
label_g.grid(row=2)
label_v.grid(row=3)
label_r.grid(row=4)
label_c.grid(row=5)

label_area.grid(row=2, column=2)
label_diam.grid(row=3, column=2)

label_foot.grid(row=7, columnspan=3)

entry_m.grid(row=1, column=1)
entry_g.grid(row=2, column=1)
entry_v.grid(row=3, column=1)
entry_r.grid(row=4, column=1)
entry_c.grid(row=5, column=1)

btn_calc.grid(row=6, columnspan=3)

root.mainloop()
