import board
import busio
import adafruit_rfm69
import adafruit_bus_device
import digitalio
import analogio
import time
import adafruit_dht
import adafruit_bmp280
import adafruit_gps
import adafruit_ccs811

# define gps
RX = board.RX
TX = board.TX
uart = busio.UART(TX, RX, baudrate=9600, timeout=2)
gps = adafruit_gps.GPS(uart, debug=False)
gps.send_command(b"PMTK314,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0")
gps.send_command(b"PMTK220,700")
last_print = time.monotonic()

# define battery pin
bat = analogio.AnalogIn(board.D9)

# define sensors
uv = analogio.AnalogIn(board.A1)
sp = analogio.AnalogIn(board.A2)
dht = adafruit_dht.DHT22(board.D10)

i2c = busio.I2C(board.SCL, board.SDA)
bmp280 = adafruit_bmp280.Adafruit_BMP280_I2C(i2c)
bmp280.sea_level_pressure = 1015

ccs = adafruit_ccs811.CCS811(i2c)
temp = ccs.temperature
ccs.temp_offset = temp - 25.0

# define rfm69hcw radio
spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
cs = digitalio.DigitalInOut(board.D5)
reset = digitalio.DigitalInOut(board.D6)
rfm69 = adafruit_rfm69.RFM69(spi, cs, reset, 433.0)
rfm69.tx_power = 20
rfm69.encryption_key = b"\x04\x04\x04\x04\x04\x04\x04\x04\x01\x01\x01\x01\x01\x01\x01\x01"

trig = digitalio.DigitalInOut(board.D11)
trig.direction = digitalio.Direction.OUTPUT
trig.value = True

global count
count = 0

while True:
    count += 1
    if count % 10 == 0:
        trig.value = False
        time.sleep(0.06)
        trig.value = True
    # get gps coords
    try:
        gps.update()
    except:
        print("gps update error")
    current = time.monotonic()
    if current - last_print >= 1.0:
        last_print = current
        '''
        if not gps.has_fix:
            # Try again if we don't have a fix yet.
            print("Waiting for GPS fix...")
            continue
        '''

    # get gps coords
    lat = gps.latitude
    long = gps.longitude

    try:
        speed = round(gps.speed_knots, 2)
    except:
        speed = None

    # get sensor values
    try:
        temp = round((bmp280.temperature + dht.temperature) / 2, 1)
    except:
        temp = round(bmp280.temperature, 1)
    try:
        hum = dht.humidity
    except:
        print("dht error")

    pressure = round(bmp280.pressure, 1)
    uvread = round((uv.value * 3.3) / 65536, 2)
    solarvolt = round((sp.value * 3.3) / 65536, 1)
    batvolt = round((bat.value * 3.3)*2 / 65536, 1)
    try:
        alt = round(gps.altitude_m, 2)
    except:
        alt = round(bmp280.altitude, 1)

    c02 = ccs.eco2

    # create data packet
    try:
        data = [
            "D",
            str(temp),
            str(pressure),
            str(hum),
            str(uvread),
            str(lat),
            str(long),
            str(alt),
            str(solarvolt),
            str(batvolt),
            str(speed),
            str(c02)
        ]
    except:
        data = ['error', 'making', 'packet']
    packet = ",".join(data)
    print(packet)
    '''
    with open("log.csv", "a+") as log:
        log.write(packet + '\n')
        log.flush()
    '''
    # send packet (and await confirmation)
    try:
        rfm69.send(str(packet))
    except:
        print("error sending packet")

    """
    packet = rfm69.receive(timeout=0.5)
    if packet is None:
        print('Received nothing! Listening again...\n')
    else:
        packet_text = str(packet, 'ascii')
        print('Received: {0} \n'.format(packet_text))
    """
    time.sleep(0.6)