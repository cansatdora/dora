import matplotlib.pyplot as plt
import matplotlib.animation as anim
import serial
import time
import random
import tkinter as tk
from tkinter import *
from PIL import Image, ImageTk

t = []
p = []
h = []
a = []
u = []
s = []

fig = plt.figure(figsize=(9,4), num='DORA live graphs')
tp = fig.add_subplot(3,2,1)
pp = fig.add_subplot(3,2,2)
hp = fig.add_subplot(3,2,3)
up = fig.add_subplot(3,2,4)
ap = fig.add_subplot(3,2,5)
sp = fig.add_subplot(3,2,6)
plt.tight_layout(pad=2)
plt.style.use(['seaborn-bright'])

ser = serial.Serial('/dev/ttyACM0')
packet = (ser.readline()).decode('utf-8')

data = { "temperature" : { "input" : "Insert temperature readings: ",
                           "hu_relations" : {
                                                "A": {"min": 15   , "max": 35, "mult": 0   , "add": 10 },
                                                "B": {"min": -18.3, "max": 15, "mult": 0.3 , "add": 5.5 },
                                                "C": {"min": 35   , "max": 50, "mult": -0.67, "add": 33.3}
                                          },
                           "pl_relations" : {
                                                "A": {"min": 10, "max": 25, "mult": 0, "add": 10 },
                                                "B": {"min": 0, "max": 10, "mult": 0.7, "add": 3 },
                                                "C": {"min": -125, "max": 0, "mult": 0.024, "add": 3 },
                                                "D": {"min": 25, "max": 70, "mult": -0.16, "add": 13.89 },
                                                "E": {"min": 70, "max": 99, "mult": -0.1, "add": 10.24 }
                           },
                           "value" : "",
                           "hu_range" : "X",
                           "pl_range" : "X",
                           "hu_points" : 0,
                           "pl_points" : 0,
                           },
         "pressure" : {"input" : "Insert pressure readings: ",
                       "hu_relations" : {
                                                "A": {"min": 500 , "max": 900  , "mult": 0.025   , "add": -12.5 },
                                                "B": {"min": 900 , "max": 1100 , "mult": 0      , "add": 10 },
                                                "C": {"min": 1100, "max": 10000, "mult": -0.0011, "add": 11.24 }
                                      },
                       "pl_relations" : {
                                               "A": {"min": 6, "max": 700, "mult": 0.014, "add": -0.086 },                                               "B": {"min": 700, "max": 1300, "mult": 0, "add": 10 , "comment": "" },
                                               "C": {"min": 1300, "max": 3000, "mult": -0.0041, "add": 15.35 },
                                               "D": {"min": 3000, "max": 5000, "mult": -0.0015, "add": 7.5 }
                       },
                       "value" : "",
                       "hu_range" : "X",
                       "pl_range" : "X",
                       "hu_points" : 0,
                       "pl_points" : 0
                       },
         "uv" : {"input" : "Insert UV readings: ",
                 "hu_relations" : {
                                                "A": {"min": 0  , "max": 12, "mult": -0.8, "add": 10.8 },
                                                "B": {"min": 12 , "max": 60, "mult": 0   , "add": 1  }
                                },
                 "pl_relations" : {
                                                "A": {"min": 0 , "max": 4, "mult": 0.67, "add": 7.33 },
                                                "B": {"min": 4, "max": 4, "mult": 0, "add": 10 },
                                                "C": {"min": 4, "max": 10, "mult": -1, "add": 13 },
                                                "D": {"min": 10, "max": 60, "mult": 0, "add": 0 }
                 },
                 "value" : "",
                 "hu_range" : "X",
                 "pl_range" : "X",
                 "hu_points" : 0,
                 "pl_points" : 0
                  },
         "humidity" : {"input" : "Insert humidity readings: ",
                       "hu_relations" : {
                                                "A": {"min": 0 , "max": 45 , "mult": 0.11 , "add": 3     },
                                                "B": {"min": 45, "max": 55 , "mult": 0    , "add": 8     },
                                                "C": {"min": 55, "max": 100, "mult": -0.11, "add": 14.11 }
                                      },
                       "pl_relations" : {
                                                "A": {"min": 0, "max": 40, "mult": 0.22, "add": 1 },
                                                "B": {"min": 40, "max": 60, "mult": 0, "add": 10  },
                                                "C": {"min": 60, "max": 100, "mult": -0.15, "add": 19 }
                       },
                       "value" : "",
                       "hu_range" : "X",
                       "pl_range" : "X",
                       "hu_points" : 0,
                       "pl_points" : 0
                       },
         "CO2" : {"input" : "Insert co2: ",
                  "hu_relations" : {
                                                "A": {"min": 0  , "max": 300, "mult": 0, "add": 10 },
                                                "B": {"min": 300 , "max": 1500, "mult": -0.0017, "add": 10.5 },
                                                "C": {"min": 1500 , "max": 3500, "mult": -0.0005, "add": 8.75 },
                                                "D": {"min": 3500 , "max": 40000, "mult": -0.00019, "add": 7.67}
                                },
                  "pl_relations" : {
                                                "A": {"min": 150 , "max": 300, "mult": 0.047, "add": -7 },
                                                "B": {"min": 300 , "max": 1000000 , "mult": 0.00021, "add": 6.94}
                 },
                  "value" : "",
                  "hu_range" : "X",
                  "pl_range" : "X",
                  "hu_points" : 0,
                  "pl_points" : 0
                  }
        }

def insert(joker):
    try:
        joker["value"] = float(input(joker["input"]))
        return joker
    except ValueError:
        print("Your input is not viable! \nTry again")
        insert(joker)
        return joker

def hum_points(joker):
    for x in joker["hu_relations"]:
        if joker["hu_relations"][x]["min"] <= joker["value"] <= joker["hu_relations"][x]["max"]:
            joker["hu_range"] = x
            joker["hu_points"] = joker["hu_relations"][x]["mult"] * joker["value"] + joker["hu_relations"][x]["add"]
    return joker

def human_index(joker):
    hu_points = 0
    for x in joker:
        if joker[x]["hu_range"] == "X":
            hu_points = 0
            break
        else:
            hu_points += joker[x]["hu_points"]
    hu_points = round(hu_points / len(joker), 2)
    return hu_points

def pla_points(joker):
    for x in joker["pl_relations"]:
        if joker["pl_relations"][x]["min"] <= joker["value"] <= joker["pl_relations"][x]["max"]:
            joker["pl_range"]= x
            joker["pl_points"] = joker["pl_relations"][x]["mult"] * joker["value"] + joker["pl_relations"][x]["add"]
    return joker

def plant_index(joker):
    pl_points = 0
    for x in joker:
        if joker[x]["pl_range"] == "X":
            pl_points = 0
            break
        else:
            pl_points += joker[x]["pl_points"]
    pl_points = round(pl_points / len(joker), 2)
    return pl_points

def dora_index(joker):
    result = round((human_index(joker) + plant_index(joker)) / 2, 2)
    return result

root = tk.Tk()
root.geometry('500x700')
root.title('Di+ and telemetry')
root.configure(background='white')
titlelbl = tk.Label(root, text= "DORA index +", font= ('Verdana', 30), bg='white')
titlelbl.pack()
scrollbar = Scrollbar(root)
scrollbar.pack(side=RIGHT, fill=Y)
listbox = Listbox(root)
listbox.pack(expand=1,fill='both')
batlbl = tk.Label(root, font=('Verdana', 25), bg='white')
altlbl = tk.Label(root, font=('Verdana', 25), bg='white')
splbl = tk.Label(root, font=('Verdana', 25), bg='white')
sollbl = tk.Label(root, font=('Verdana', 20), bg='white')
timelbl =  tk.Label(root, font=('Verdana', 20), bg='white')

# imgpath = Image.open("0.png")
img = PhotoImage(file = '0.png')
imglbl = tk.Label(root, image=img, font=('Verdana', 25), bg='white')

def update(i):
    packet = (ser.readline()).decode('utf-8')
    print(str(packet))
    if "D" in packet:
        packet = packet[:-1]
        can = packet.split(',')
        print(str(can))
        temp = float(can[1])
        print("temp: %s" % temp)
        pres = float(can[2])
        print("pres: %s" % pres)
        hum = float(can[3])
        print("hum: %s" % hum)
        uv = float(can[4])
        print("uv: %s" % uv)
        alt = float(can[7])
        sol = float(can[8])
        bat = float(can[9])
        cc = float(can[11])
        print("co2: %s" % cc)
        try:
            speed = float((data[10]).rstrip())
            speed *= 0.51
        except:
            speed = 0
        try:
            lat = float(can[5])
            long = float(can[6])
            position = str(long)+','+str(lat)+','+str(alt)
            print('position: %s' % position)
            print('\n')

            try:
                with open ("placemark.kml", "w") as pm:
                    pm.write("""<?xml version="1.0" encoding="UTF-8"?>
                    <kml xmlns="http://www.opengis.net/kml/2.2">
                    <Placemark>
                    <name>DORA</name>
                    <description>DORA CanSat live location</description>
                    <visibility>1</visibility>
                    <Style>
                    <IconStyle>
                    <Icon>
                    <href>http://maps.google.com/mapfiles/kml/paddle/wht-blank.png</href>
                    </Icon>
                    </IconStyle>
                    </Style>
                    <Point>
                    <altitudeMode>absolute</altitudeMode>
                    <coordinates>%s,%s,%s</coordinates>
                    </Point>
                    </Placemark>
                    </kml>""" % (can[6], can[5], alt))
                with open ('coords.txt', 'a+') as co:
                    co.write(position + '\n')
                with open('path.kml', "w") as pt:
                    lines = open('coords.txt', 'r').read()
                    pt.write('''<?xml version="1.0" encoding="UTF-8"?>
                    <kml xmlns="http://www.opengis.net/kml/2.2">
                    <Document>
                    <name>DORA flight path</name>
                    <Style id="yellowLineGreenPoly">
                    <LineStyle>
                    <color>7f00ffff</color>
                    <width>4</width>
                    </LineStyle>
                    <PolyStyle>
                    <color>7f00ff00</color>
                    </PolyStyle>
                    </Style>
                    <Placemark>
                    <styleUrl>#yellowLineGreenPoly</styleUrl>
                    <LineString>
                    <extrude>1</extrude>
                    <tessellate>1</tessellate>
                    <altitudeMode>absolute</altitudeMode>
                    <coordinates>%s

                    </coordinates>
                    </LineString>
                    </Placemark>
                    </Document>
                    </kml>''' % (lines))
            except:
                print('path error')
        except:
            print('incomplete location data')


    try:
        t.append(temp)
        p.append(pres)
        h.append(hum)
        a.append(alt)
        s.append(speed)
        u.append(cc)
    except:
        print('graph value error')

    xt = range(len(t))
    xp = range(len(p))
    xh = range(len(h))
    xu = range(len(u))
    xa = range(len(a))
    xs = range(len(s))

    tp.clear()
    tp.plot(xt, t, color = 'red', marker = 'o', markersize = '2')
    tp.set_ylim(20, 30)
    tp.set_title('Temperature = %s *C' % temp)
    tp.set_ylabel('*C')
    '''
    try:
        tp.annotate(str(temp),xy=(float(xt[-1]), float(temp)))
    except:
        print('tp anot er')
    '''
    pp.clear()
    pp.plot(xp, p, color = 'blue', marker = 's', markersize = '2')
    pp.set_ylim(500, 1100)
    pp.set_title('Pressure = %s hPa' % pres)
    pp.set_ylabel('hPa')
    '''
    try:
        pp.annotate(str(pres),xy=(float(xp[-1]), float(pres)-20))
    except:
        print('pp anot er')
    '''

    hp.clear()
    hp.plot(xh, h, color = 'green', marker = 'o', markersize = '2')
    hp.set_ylim(10, 90)
    hp.set_title('Humidity = %s' % hum)
    hp.set_ylabel('%')
    '''
    try:
        hp.annotate(str(hum),xy=(float(xh[-1]), float(hum)))
    except:
        print('hp anot er')
    '''

    up.clear()
    up.plot(xu, u, color = 'pink', marker = 'o', markersize = '2')
    up.set_ylim(0, 10000)
    up.set_title('CO2 = %s ppm' % cc)
    up.set_ylabel('ppm')


    ap.clear()
    ap.plot(xa, a, color = 'purple', marker = 'v', markersize = '2')
    ap.set_ylim(0, 1050)
    ap.set_title('Altitude = %s m   ' % alt)
    ap.set_ylabel('m')
    '''
    try:
        ap.annotate(str(alt),xy=(float(xa[-1]), float(alt)))
    except:
        print('ap anot er')
    '''
    try:
        sp.clear()
        sp.plot(xs, s, color = 'gray', marker = 'v', markersize = '2')
        sp.set_ylim(0, 20)
        sp.set_title('Speed = %s m/s' % speed)
        sp.set_ylabel('m/s')
    except:
        print('speed unavail')

    data["temperature"]["value"] = temp
    data["pressure"]["value"] = pres
    data["humidity"]["value"] = hum
    data["uv"]["value"] = uv
    try:
        data["co2"]["value"] = cc
    except:
        data["co2"]["value"] = 1000
    #try:
    listbox.insert(END, packet)
    #except:
        #print('incomplete data')
    for x in data:
        hum_points(data[x])
        pla_points(data[x])

    human_points = (human_index(data))
    plant_points = (plant_index(data))
    score = dora_index(data)

    listbox.insert(END, "DORA index score is: " + str(score))
    titlelbl['text'] = "DORA index + = %s/10" % str(score)
    titlelbl.pack()

    batlbl['text'] = 'Battery voltage = %s V' % str(bat)
    altlbl['text'] = 'Altitude = %s m' % str(alt)
    splbl['text'] = 'Speed = %s m/s' % str(speed)
    sollbl['text'] = 'Solar panel voltage = %s V' % str(sol)
    try:
        timelbl['text'] = 'ETF = %s s' % str(1000/speed)
    except:
        timelbl['text'] = 'ETF unavailable'

    with open ("log.csv", "a") as log:
        log.write((str(packet)).rstrip() + ',' + str(score) + '\n')

    try:
        volt_dec = bat-3
        volt_per = volt_dec/1.2
        bat_per = round(volt_per * 100)
        batlbl['text'] = 'Battery level = ' + str(bat_per) + '%'
        print('bat = %s' % round(bat_per))

    except:
        print('error reading bat level')

    imglbl.pack()
    batlbl.pack()
    sollbl.pack()
    altlbl.pack()
    splbl.pack()
    try:
        if speed > 2:
            timelbl.pack()
    except:
        print('ETF unavail')
    root.update()
	# plt.subplots_adjust(bottom = 0.1)

animation = anim.FuncAnimation(fig, update)
plt.show()
