import processing.serial.*;

//init variables
Serial commPort;
float pressure;
float temp;
float hum;
int yDist1;
int yDist2;
int yDist3;
float[] pressureHistory = new float[100];
float[] tempHistory = new float[100];
float[] humHistory = new float[100];

PImage dora;
PrintWriter data;

void setup()
{
  size(980, 230);
  data = createWriter("data.txt");
  background(150);
  //init serial communication port
  commPort = new Serial(this, "COM11", 115200);
  //fill pressureHistory with 0s
  for (int index = 0; index<100; index++) {
    pressureHistory[index] = 0;
    tempHistory[index] = 0;
    humHistory[index] = 0;
  }
}

void draw()
{

  while (commPort.available()>0) {
    delay(100);
    String packet = commPort.readString();
    println(packet);

    if ((packet.substring(0, 6)).equals("[DATA]")) {
      String len = packet.substring(11, 13);
      println(len);

      float hum = int(packet.substring(14, 16));
      println(hum);

      float temp = float((packet.substring(17, 21)));
      println(temp);

      float pressure = float(packet.substring(22, 28).replace('|', '\0'));
      println(pressure);

      data.println(packet);

      if (temp>0 && temp<27) {
        //refresh the background to clear old data
        background(150); 

        //draw the temp rectangle
        colorMode(RGB, 160);  //use color mode sized for fading
        stroke (0);
        rect (49, 19, 22, 162);
        //fade red and blue within the rectangle
        for (int colorIndex = 0; colorIndex <= 160; colorIndex++) 
        {
          stroke(160 - colorIndex, 0, colorIndex);
          line(50, colorIndex + 20, 70, colorIndex + 20);
        }
        //draw graph
        stroke(0);
        fill(255, 255, 255);
        rect(90, 80, 100, 100);
        for (int index = 0; index<100; index++)
        { 
          if (index == 99)
            tempHistory[index] = temp;
          else
            tempHistory[index] = tempHistory[index + 1];
          point(90 + index, 180 - 2*tempHistory[index]);
        }
        //write reference values
        fill(0, 0, 0);
        textAlign(RIGHT);
        text("50 C", 45, 25);
        text("25 C", 45, 100);
        text("0 C", 45, 187);
        //draw triangle pointer
        yDist1 = int(160 - (160 * (temp * 0.02)));
        stroke(0);
        triangle(75, yDist1 + 20, 85, yDist1 + 15, 85, yDist1 + 25);
        //write the temp in C
        fill(0, 0, 0);
        textAlign(LEFT);
        text(str(int(temp)) + " C", 115, 37);
      }


      //DRAWS PRESSURE GRAPH


      if (pressure>900 && pressure<1150) { //remove unwanted values
        //draw the pressure rectangle
        colorMode(RGB, 160);  //use color mode sized for fading
        stroke (0);
        rect (409, 19, 22, 162);
        //fade red and blue within the rectangle
        for (int colorIndex = 0; colorIndex <= 160; colorIndex++) 
        {
          stroke(160 - colorIndex, 0, colorIndex);
          line(410, colorIndex + 20, 430, colorIndex + 20);
        }
        //draw graph
        stroke(0);
        fill(255, 255, 255);
        rect(450, 80, 100, 100);
        for (int index = 0; index<100; index++)
        { 
          if (index == 99)
            pressureHistory[index] = pressure;
          else
            pressureHistory[index] = pressureHistory[index + 1];
          point(450 + index, 180 - (pressureHistory[index])*0.04);
        }
        //write reference values
        fill(0, 0, 0);
        textAlign(LEFT);
        text("1150 hPa", 400, 15); 
        text("900 hPa", 400, 195);
        //draw triangle pointer
        yDist2 = int(160-(pressure/16));
        stroke(0);
        triangle(435, yDist2 + 20, 445, yDist2 + 15, 445, yDist2 + 25);
        //write the pressure in C
        fill(0, 0, 0);
        textAlign(LEFT);
        text(str(int(pressure)) + " hPa", 475, 37);
      }
      //draws humidity graph

      if (hum>0 && hum<=100) { //remove unwanted values
        //draw the pressure rectangle
        colorMode(RGB, 160);  //use color mode sized for fading
        stroke (0);
        rect (769, 19, 22, 162);
        //fade red and blue within the rectangle
        for (int colorIndex = 0; colorIndex <= 160; colorIndex++) 
        {
          stroke(160 - colorIndex, 0, colorIndex);
          line(770, colorIndex + 20, 790, colorIndex + 20);
        }
        //draw graph
        stroke(0);
        fill(255, 255, 255);
        rect(810, 80, 100, 100);
        for (int index = 0; index<100; index++)
        { 
          if (index == 99)
            humHistory[index] = hum;
          else
            humHistory[index] = humHistory[index + 1];
          point(810 + index, 180 - humHistory[index]);
        }
        //write reference values
        fill(0, 0, 0);
        textAlign(LEFT);
        text("100%", 740, 15);
        text("50%", 740, 105);
        text("0%", 750, 195);
        //draw triangle pointer
        yDist2 = int(160-1.6*hum);
        stroke(0);
        triangle(795, yDist2 + 20, 805, yDist2 + 15, 805, yDist2 + 25);
        //write the pressure in C
        fill(0, 0, 0);
        textAlign(LEFT);
        text(str(int(hum)) + " % Humidity", 830, 37);
      }
    }
  }
  if (keyPressed) {
    if (key == 'q' || key == 'Q') {
      data.flush();
      data.close();
      exit();
    }
  }
}
