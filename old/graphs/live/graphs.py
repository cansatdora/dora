import matplotlib.pyplot as plt
import matplotlib.animation as anim
import socket
import time
t = []
p = []
a = []

fig = plt.figure(figsize=(6,4), num='DORA live graphs')
tp = fig.add_subplot(5,1,1)
pp = fig.add_subplot(5,1,3)
ap = fig.add_subplot(5,1,5)
plt.style.use(['seaborn-bright'])


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('169.254.212.109', 50000))
global data

packet = s.recv(1024)

def update(i):
    packet = s.recv(1024)
    if "DORA" in packet:
        data = packet.split(',')
        print(str(data))
        with open ("log.csv", "a+") as log:
            log.write(str(packet))
        try:
            alt = float(data[7])
            print("gps alt: %s" % alt)
        except:
            alt = float(data[8])
            print("bmp alt: %s" % alt)
        try:
            temp = float(data[1])
            print("temp: %s" % temp)
            pres = float(data[2])
            print("pres: %s" % pres)
            hum = float(data[3])
            print("hum: %s" % hum)
            lat = float(data[5])
            print("lat: %s" % lat)
            long = float(data[6])
            print("long: %s" % long)
            position = str(long)+','+str(lat)+','+str(alt)
            print(position)

            print('\n')
        except:
            print('location error')

        try:
            with open ("placemark.kml", "w") as pm:
                pm.write("""<?xml version="1.0" encoding="UTF-8"?>
                <kml xmlns="http://www.opengis.net/kml/2.2">
                  <Placemark>
                    <name>DORA</name>
                    <description>DORA CanSat live location</description>
                    <visibility>1</visibility>
                    <Point>
                      <coordinates>%s,%s,%s</coordinates>
                    </Point>
                  </Placemark>
                </kml>""" % (data[6], data[5], alt))
            with open ('coords.txt', 'a+') as co:
                co.write(position + '\n')
            with open('path.kml', "w") as pt:
                lines = open('coords.txt', 'r').read()
                pt.write('''<?xml version="1.0" encoding="UTF-8"?>
                <kml xmlns="http://www.opengis.net/kml/2.2">
                  <Document>
                    <name>DORA flight path</name>
                    <Style id="orange-5px">
                      <LineStyle>
                        <color>ff00aaff</color>
                        <width>5</width>
                      </LineStyle>
                    </Style>
                    <Placemark>
                      <styleUrl>#orange-5px</styleUrl>
                      <LineString>
                        <tessellate>1</tessellate>
                        <altitudeMode>absolute</altitudeMode>
                        <coordinates>%s

                        </coordinates>
                      </LineString>
                    </Placemark>
                  </Document>
                </kml>''' % (lines))
        except:
            print('path error')
    try:
        t.append(temp)
        p.append(pres)
        a.append(alt)
    except:
        print('error graphing data')
    xt = range(len(t))
    xp = range(len(p))
    xa = range(len(a))

    try:
        tp.clear()
        tp.plot(xt, t, color = 'red', marker = 'o', markersize = '2')
        tp.set_ylim(20, 30)
        tp.set_title('Temperature = %s *C' % temp)
        tp.set_ylabel('*C')
        try:
            tp.annotate(str(temp),xy=(float(xt[-1]), float(temp)))
        except:
            print('tp anot er')

        pp.clear()
        pp.plot(xp, p, color = 'blue', marker = 's', markersize = '2')
        pp.set_ylim(980, 1030)
        pp.set_title('Pressure = %s hPa' % pres)
        pp.set_ylabel('hPa')
        try:
            pp.annotate(str(pres),xy=(float(xp[-1]), float(pres)-20))
        except:
            print('pp anot er')

        ap.clear()
        ap.plot(xa, a, color = 'purple', marker = 'v', markersize = '2')
        ap.set_ylim(0, 1050)
        ap.set_title('Altitude = %s m   ' % alt)
        ap.set_ylabel('m')
        try:
            ap.annotate(str(alt),xy=(float(xa[-1]), float(alt)))
        except:
            print('ap anot er')
    except:
        print('graph er')

    # plt.subplots_adjust(bottom = 0.1)

animation = anim.FuncAnimation(fig, update)
plt.show()
