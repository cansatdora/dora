import matplotlib.pyplot as plt
import matplotlib.animation as anim
import serial

p = []
fig = plt.figure()
ax = fig.add_subplot(1,1,1)

ser = serial.Serial("/dev/ttyACM0")
global data
data = [0,0,0,0]
packet = (ser.readline()).decode("utf-8")

def update(i):
    packet = (ser.readline()).decode("utf-8")
    if "DORA" in packet:
        data = packet.split(',')
        print(data)
        pres = float(data[2])
        print(pres)

    p.append(pres)
    x = range(len(p))
    ax.clear()
    ax.plot(x, p)
    ax.set_ylim(950,1050)

a = anim.FuncAnimation(fig, update)
plt.show()
