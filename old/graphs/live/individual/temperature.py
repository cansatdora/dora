import matplotlib.pyplot as plt
import matplotlib.animation as anim
import serial
import time

t = []
fig = plt.figure()
ax = fig.add_subplot(1,1,1)

ser = serial.Serial("/dev/ttyACM0")
global data
global temp
data = [0,0,0,0]
packet = (ser.readline()).decode("utf-8")

def update(i):
    packet = (ser.readline()).decode("utf-8")
    if "DORA" in packet:
        data = packet.split(',')
        print(data)
        temp = float(data[1])
        print(temp)
    t.append(temp)
    x = range(len(t))
    ax.clear()
    ax.plot(x, t)
    ax.set_ylim(18,25)
    time.sleep(1)

a = anim.FuncAnimation(fig, update)
plt.show()
