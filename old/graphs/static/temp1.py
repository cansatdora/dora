import matplotlib.pyplot as plt
import serial
import pandas as pd
import numpy as np

dataset = pd.read_csv('test_data.csv',
                    names=["time", "id", "temp", "hum", "pres", "uv", "alt"])
dataset = dataset.drop(columns=['time', 'id'])

temperature = np.array(dataset['temp'])
print(temperature)

size = str(temperature.shape)
size = int(size[1:4])
x = np.arange(size)

fig, ax = plt.subplots()
ax.plot(x, temperature)

ax.set(xlabel='time (s)', ylabel='voltage (mV)',
       title='About as simple as it gets, folks')
ax.grid()

plt.ylim(10,30)
fig.savefig("test.png")
plt.show()
