#include <SPI.h>
#include <RH_RF69.h>

#define RF69_FREQ 433.0

#define RFM69_INT     3
#define RFM69_CS      4
#define RFM69_RST     2
#define LED           13

RH_RF69 rf69(RFM69_CS, RFM69_INT);

int16_t packetnum = 0;

void setup() 
{
  Serial.begin(9600);

  pinMode(LED, OUTPUT);     
  pinMode(RFM69_RST, OUTPUT);
  digitalWrite(RFM69_RST, LOW);

  // manual reset
  digitalWrite(RFM69_RST, HIGH);
  delay(10);
  digitalWrite(RFM69_RST, LOW);
  delay(10);
  
  rf69.init();
  
  rf69.setFrequency(RF69_FREQ);  

  rf69.setTxPower(20, true);

  uint8_t key[] = { 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04,
                    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01};
  rf69.setEncryptionKey(key);
  
  pinMode(LED, OUTPUT);
}

void loop() {
 if (rf69.available()) {
    // Should be a message for us now   
    uint8_t buf[RH_RF69_MAX_MESSAGE_LEN];
    uint8_t len = sizeof(buf);
    if (rf69.recv(buf, &len)) {
      if (!len) return;
      buf[len] = 0;
      Serial.print(String((char*)buf) + '\n');
      
      uint8_t data[] = "ACK";
      rf69.send(data, sizeof(data));
      rf69.waitPacketSent();
    }
  }
}
