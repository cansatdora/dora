import socket
import serial
import Tkinter as tk
# from PIL import Image, ImageTk

ser = serial.Serial('/dev/ttyACM0')
packet = (ser.readline()).decode('utf-8')
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('192.168.43.127', 10000))
s.listen(1)
conn, addr = s.accept()

root = tk.Tk()
root.geometry('640x470')
root.title('DORA TELEMETRY')
root.configure(background='white')


canvas = tk.Canvas(root, width=640, height=470, bg='white')

batlbl = tk.Label(root, font=('System', 25), bg='white')
altlbl = tk.Label(root, font=('System', 25), bg='white')
splbl = tk.Label(root, font=('System', 25), bg='white')
sollbl = tk.Label(root, font=('System', 20), bg='white')
timelbl =  tk.Label(root, font=('System', 35), bg='white')


while 1:
    packet = (ser.readline()).decode('utf-8')
    if "D" in packet:
        data = packet.split(',')
        alt = data[7]
        bat = data[9]
        sol = data[8]
        try:
            speed = float((data[10]).rstrip())
            speed *= 0.51
        except:
            speed = 0
        print(str(data))
        conn.sendall(packet)
        batlbl['text'] = 'Battery voltage = %s V' % str(bat)
        altlbl['text'] = 'Altitude = %s m' % str(alt)
        splbl['text'] = 'Speed = %s m/s' % str(speed)
        sollbl['text'] = 'Solar panel voltage = %s V' % str(sol)
        if speed is not None:
            timelbl['text'] = 'ETF = %s s' % str(1000*speed)

    batlbl.pack()
    altlbl.pack()
    splbl.pack()
    sollbl.pack()
    try:
        if speed > 2:
            timelbl.pack()
    except:
        print('ETF unavail')
    root.update()
conn.close()
